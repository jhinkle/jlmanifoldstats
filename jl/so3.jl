abstract SO3 <: BiinvariantRiemannianLieGroup
# Left and Right invariant versions
abstract LeftInvSO3 <: LeftInvLieGroup
abstract RightInvSO3 <: RightInvLieGroup

abstract so3 <: LieAlgebra{SO3},LieAlgebra{LeftInvSO3},LieAlgebra{RightInvSO3}

# Matrix representation of an SO(3) element
type SO3Point <: ManifoldPoint{SO3},GroupElement{SO3}
    p::Matrix
end
# Group multiplication (get left and right translation for free)
*(R::SO3Point,S::SO3Point) = SO3Point(R.p*S.p)
# Group inverse
inv(R::SO3Point) = SO3Point(inv(R.p))
# Group identity
id{G<:SO3}(::Type{G}) = SO3Point(eye(3))


# Tangent vector in the group
type SO3TangentVector <: TangentVector{SO3}
    v::Matrix
    SO3TangentVector() = new(zeros(3,3))
end
newTangentVector(::Type{SO3},u) = SO3TangentVector(u)
newTangentVector(::Type{SO3}) = SO3TangentVector()


# Axis angle representation
type SO3AlgebraElement <: AlgebraElement{so3}
    v::Vector
    SO3AlgebraElement(v::Array) = new(squeeze(v))
    SO3AlgebraElement() = new(zeros(3))
end
newLieAlgebraElement(::Type{SO3},v) = SO3AlgebraElement(v)
newLieAlgebraElement(::Type{SO3}) = SO3AlgebraElement()
# Biinvariant metric is identity
dot(U::SO3AlgebraElement,V::SO3AlgebraElement) = dot(U.v,V.v)


function ad(X::SO3AlgebraElement,Y::SO3AlgebraElement)
    SO3AlgebraElement(cross(X.v,Y.v))
end
# With the usual biinvariant metric this is just ad
coad(X::SO3AlgebraElement,Y::SO3AlgebraElement) = ad(X,Y)


# Left invariant stuff
type LeftInvSO3Point{A::Matrix} <: ManifoldPoint{LeftInvSO3}
    p::Matrix
end
# Group multiplication (get left and right translation for free)
*(R::LeftInvSO3Point,S::LeftInvSO3Point) = LeftInvSO3Point(R.p*S.p)
# Group inverse
inv(R::LeftInvSO3Point) = LeftInvSO3Point(inv(R.p))
# Group identity
id{G<:LeftInvSO3}(::Type{G}) = LeftInvSO3Point(eye(3))


# Axis angle representation
type LeftInvSO3AlgebraElement{A::Matrix} <: AlgebraElement{so3}
    v::Vector
    LeftInvSO3AlgebraElement(v::Array) = new(squeeze(v))
    LeftInvSO3AlgebraElement() = new(zeros(3))
end
newLieAlgebraElement(::Type{LeftInvSO3},v) = LeftInvSO3AlgebraElement(v)
newLieAlgebraElement(::Type{LeftInvSO3}) = LeftInvSO3AlgebraElement()
# Inner product with metric
dot{A::Matrix}(U::LeftInvSO3AlgebraElement{A},V::LeftInvSO3AlgebraElement{A}) = U.v'*A*V.v
