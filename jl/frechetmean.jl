include("geometry.jl")

function frechetMean{M<:RiemannianManifold,P<:ManifoldPoint}(::Type{M},y::Vector{P}, x0::P, numSteps, stepsize)
    x = x0

    for iter = 1:numSteps
        #print("type of x is $(typeof(x)) and type of y[1] is $(typeof(y[1]))\n")
        v = newTangentBundlePoint(M,x)
        #print("v=$v and typeof(v)=$(typeof(v))\n")
        sse = 0
        for i=1:length(y)
            #print("x=$x and y[$i]=$(y[i])\n")
            l = logmap(x,y[i])
            sse += norm(l.v)^2
            #print("l=$l and typeof(l)=$(typeof(l))\n")
            v += l
        end
        x = expmap(stepsize*v/length(y))
        print("Iter $iter of $numSteps: SSE=$sse x=$x\n")
    end
    x
end

# Default values
function frechetMean{M<:RiemannianManifold,P<:ManifoldPoint}(::Type{M},y::Vector{P}, numSteps, stepsize)
    # use first point as initial guess if not given it
    frechetMean(M,y,numSteps,stepsize)
end
function frechetMean{M<:RiemannianManifold,P<:ManifoldPoint}(::Type{M},y::Vector{P},x0::P)
    # some reasonable default optimization parameters
    frechetMean(M,y,x0,100,1)
end
function frechetMean{M<:RiemannianManifold,P<:ManifoldPoint}(::Type{M},y::Vector{P})
    # by default just choose first point as initial guess
    frechetMean(M,y,y[1])
end
