include("permtest.jl")
include("sphereregression.jl")

# for plotting
#push(LOAD_PATH,"/scratch/jacob/src/gaston/jl")
#load("gaston.jl")

#include("kendallssvis.jl")

# just sort and remove dupes
function uniqsort(A::Vector)
    A = sort(A)
    B = [A[1]]
    for x in A[2:end]
        if x != B[end]
            push(B,x)
        end
    end
    B
end

# data parameters
Npts = 100
td = [1:Npts]/Npts
# set up time discretization
Nt = 200 # actual number of timesteps will be greater than this
trange = max(td)-min(td) # overall time range
t = [min(td):trange/Nt:max(td)] # ideal discretization, without data points
t = [convert(Array{Float64},td);t] # add in the data
t = uniqsort(t) # sorted union, duplicates removed
yind = map(l->findfirst(t,l),td) # find the final indices where we have data

# generate the data
ktrue = 2
noiseSig = 0.1
vSig = 2
# set up random initial conditions
p0true = NSpherePoint([1;0;0])
v0sug = [NSphereTangentVector([0;1;0]),NSphereTangentVector([0;0;.1])]
v0true = map(i->projectTangent(p0true,v0sug[i]),[1:ktrue])
println("p0true=$p0true")
println("v0true($(size(v0true))=$v0true")
# integrate a ktrue-order curve
(ptrue,vtrue) = integratePolynomial(p0true,v0true,t)
# perturb for data
y = map(i->expmap(ptrue[yind[i]],projectTangent(ptrue[yind[i]],noiseSig*randn(3))),[1:Npts])

# find frechet mean, variance
epsmean = 0.01
Nitermean = 50 # this is fast so make sure we nailed it
(ymean,SSEmean) = frechetMean(y,epsmean,Nitermean)


# do geodesic regression
Nitergeod = 100
#Nitergeod = 1
#p0geod = y[1]
p0geod = deepcopy(ymean)
v0geod = [NSphereTangentVector(zeros(size(y[1].p)))]
#(p0geod,vi0geod,SSEgeod) = polynomialRegression(y,t,yind,p0geod,v0geod,Nitergeod)
# closed form, don't need dense t discretization
(p0geod,vi0geod,SSEgeod) = polynomialRegression(y,td,[1:Npts],p0geod,v0geod,Nitergeod)
R2geod = 1-SSEgeod[end]/SSEmean
println("Geodesic R^2 = $R2geod")
#R2predgeod = computeR2pred(y,t,yind,p0geod,vi0geod,Nitergeod)
#println("Geodesic R^2pred = $R2predgeod")
Npermsgeod = 10000
Niterpermgeod = 100
(pvalgeod,R2permgeod) = permTestAgainstMean(y,t,yind,ymean,SSEmean,1,Npermsgeod,Niterpermgeod)
println("Geodesic R^2 = $R2geod")
#println("pvalgeod=$pvalgeod\a")

# quadratic
Niterquad = 100
vi0quadinit = [vi0geod;NSphereTangentVector(zeros(size(y[1].p)))]
(p0quad,vi0quad,SSEquad) = polynomialRegression(y,t,yind,p0geod,vi0quadinit,Niterquad)
R2quad = 1-SSEquad[end]/SSEmean
println("Geodesic R^2 = $R2geod")
println("Quadratic R^2 = $R2quad")
#println("Geodesic R^2pred = $R2predgeod")
#R2predquad = computeR2pred(y,t,yind,p0quad,vi0quad,Niterquad)
#println("Quadratic R^2pred = $R2predquad")
Npermsquad = 1000
Niterpermquad = 100
(pvalquad,R2permquad) = permTestAgainstCurve(y,t,yind,p0geod,vi0geod,SSEmean,2,Npermsquad,Niterpermquad)
#println("pvalquad=$pvalquad\a")
# permutation test quadratic against quadratic (should be 50-50 right?)
#(pvalquadquad,R2permquadquad) = permTestAgainstCurve(y,t,yind,p0quad,vi0quad,SSEmean,2,Npermsquad,Niterpermquad)


# cubic
Nitercubic = 100
p0cubic = deepcopy(p0quad)
vi0cubic = [deepcopy(vi0quad);NSphereTangentVector(zeros(size(y[1].p)))]
(p0cubic,vi0cubic,SSEcubic) = polynomialRegression(y,t,yind,p0cubic,vi0cubic,Nitercubic)
R2cubic = 1-SSEcubic[end]/SSEmean
println("Geodesic R^2 = $R2geod")
println("Quadratic R^2 = $R2quad")
println("Cubic R^2 = $R2cubic")
Npermscubic = 1000
Niterpermcubic = 1000
(pvalcubic,R2permcubic) = permTestAgainstCurve(y,t,yind,p0quad,vi0quad,SSEmean,3,Npermscubic,Niterpermcubic)
println("pvalcubic=$pvalcubic\a")

# quartic
Niterquartic = 100
p0quartic = deepcopy(p0cubic)
vi0quartic = [deepcopy(vi0cubic);NSphereTangentVector(zeros(size(y[1].p)))]
(p0quartic,vi0quartic,SSEquartic) = polynomialRegression(y,t,yind,p0quartic,vi0quartic,Niterquartic)
R2quartic = 1-SSEquartic[end]/SSEmean
println("Geodesic R^2 = $R2geod")
println("Quadratic R^2 = $R2quad")
println("Cubic R^2 = $R2cubic")
println("Quartic R^2 = $R2quartic")
Npermsquartic = 1000
Niterpermquartic = 100
(pvalquartic,R2permquartic) = permTestAgainstCurve(y,t,yind,p0cubic,vi0cubic,SSEmean,4,Npermsquartic,Niterpermquartic)

# quintic
Niterquintic = 100
p0quintic = deepcopy(p0quartic)
vi0quintic = [deepcopy(vi0quartic);NSphereTangentVector(zeros(size(y[1].p)))]
(p0quintic,vi0quintic,SSEquintic) = polynomialRegression(y,t,yind,p0quintic,vi0quintic,Niterquintic)
R2quintic = 1-SSEquintic[end]/SSEmean
println("Geodesic R^2 = $R2geod")
println("Quadratic R^2 = $R2quad")
println("Cubic R^2 = $R2cubic")
println("Quartic R^2 = $R2quartic")
println("Quintic R^2 = $R2quintic")
Npermsquintic = 1000
Niterpermquintic = 100
(pvalquintic,R2permquintic) = permTestAgainstCurve(y,t,yind,p0quartic,vi0quartic,SSEmean,5,Npermsquintic,Niterpermquintic)


## summarize results
println("Geodesic R^2 = $R2geod")
println("Quadratic R^2 = $R2quad")
println("Cubic R^2 = $R2cubic")
println("Quartic R^2 = $R2quartic")
println("Quintic R^2 = $R2quintic")
println("pvalgeod=$pvalgeod\a")
println("pvalquad=$pvalquad\a")
println("pvalcubic=$pvalcubic\a")
println("pvalquartic=$pvalquartic\a")
println("pvalquintic=$pvalquintic\a")
