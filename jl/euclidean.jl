include("geometry.jl")

abstract Euclidean <: RiemannianManifold

type EuclideanPoint <: ManifoldPoint{Euclidean}
    p::Vector
    EuclideanPoint(u::Array) = new(squeeze(u))
    EuclideanPoint() = new([0,0,0])
end
newManifoldPoint(::Type{Euclidean},p) = EuclideanPoint(p)

type EuclideanTangentVector <: TangentVector{Euclidean}
    v::Vector
    EuclideanTangentVector(u::Array) = new(squeeze(u))
    EuclideanTangentVector() = new([0,0,0])
end
newTangentVector(::Type{Euclidean},u) = EuclideanTangentVector(u)
newTangentVector(::Type{Euclidean}) = EuclideanTangentVector()

type EuclideanTangentBundlePoint <: TangentBundlePoint{Euclidean}
    p::EuclideanPoint
    v::EuclideanTangentVector
    EuclideanTangentBundlePoint(p::Array,v::Array) = new(EuclideanPoint(p),EuclideanTangentVector(v))
    EuclideanTangentBundlePoint(p::EuclideanPoint) = new(p,EuclideanTangentVector())
end
EuclideanTangentVector(pv::EuclideanTangentBundlePoint) = EuclideanTangentVector(pv.v.v)
newTangentBundlePoint(::Type{Euclidean},p,v) = EuclideanTangentBundlePoint(p,v)
newTangentBundlePoint(::Type{Euclidean},p) = EuclideanTangentBundlePoint(p)

function dot{T<:EuclideanTangentVector}(a::T,b::T)
    dot(a.v,b.v)
end
function norm{T<:EuclideanTangentVector}(a::T)
    norm(a.v)
end


function distance{M<:Euclidean}(a::ManifoldPoint{M},b::ManifoldPoint{M})
    norm(a.p-b.p) # can compute this without using log map for Euclidean
end


function expmap{M<:Euclidean}(p::ManifoldPoint{M},v::TangentVector{M})
    newManifoldPoint(M,p.p+v.v)
end
function expmap{M<:Euclidean}(pv::TangentBundlePoint{M})
    expmap(pv.p,pv.v)
end


function logmap{M<:Euclidean}(p::ManifoldPoint{M},q::ManifoldPoint{M})
    newTangentBundlePoint(M,p.p,q.p-p.p)
end


function partrans{M<:Euclidean}(p::ManifoldPoint{M},v::TangentVector{M},w::TangentVector{M})
    # parallel translation is trivial in Euclidean space
    newTangentVector(M,w)
end


# specialize some more expensive algorithms
function frechetMean(::Type{Euclidean},y::Vector{EuclideanPoint},x0::EuclideanPoint)
    # note this only gets dispatched if we DON'T give numSteps and stepsize
    print("Computing non-iterative Euclidean mean\n")
    mean(map(v->v.p,y))
end

