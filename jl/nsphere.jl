type NSpherePoint
    p::Vector
    function NSpherePoint(p::Array)
        if abs(Base.norm(p[:])-1) > 1e-8
            error("Point on the sphere must have unit norm")
        end
        new(p)
    end
    function NSpherePoint(p::NSpherePoint)
        new(p.p)
    end
end
function projectToSphere(p::Array)
    NSpherePoint(p/Base.norm(p))
end

type NSphereTangentVector
    v::Vector
    function NSphereTangentVector(v::Vector)
        new(v)
    end
    function NSphereTangentVector(v::NSphereTangentVector)
        new(v.v)
    end
end
function dot(u::NSphereTangentVector,v::NSphereTangentVector)
    Base.dot(u.v,v.v)
end
function +(u::NSphereTangentVector,v::NSphereTangentVector)
    NSphereTangentVector(u.v+v.v)
end
function -(u::NSphereTangentVector,v::NSphereTangentVector)
    NSphereTangentVector(u.v-v.v)
end
function *(a::Number,v::NSphereTangentVector)
    NSphereTangentVector(a*v.v)
end
function *(v::NSphereTangentVector,a::Number)
    a*v
end
function /(v::NSphereTangentVector,a)
    NSphereTangentVector(v.v/a)
end
function projectTangent(p::NSpherePoint, v::Vector)
    NSphereTangentVector(v - Base.dot(p.p,v)*p.p)
end
function projectTangent(p::NSpherePoint, v::NSphereTangentVector)
    projectTangent(p,v.v)
end

type NSphereTangentBundlePoint
    p::NSpherePoint
    v::NSphereTangentVector
    function NSphereTangentBundlePoint(p,v)
        # TODO: Check that p and v are orthogonal
        p = NSpherePoint(p)
        v = NSphereTangentVector(v)
        if abs(Base.dot(p.p,v.v)) > 1e-10
            error("Point and vector must be orthogonal for N-sphere tangent bundle point")
        end
        new(p,v) 
    end
end

function expmap(p::NSpherePoint,v::NSphereTangentVector)
    # find angle
    theta = Base.norm(v.v)
    if theta < 1e-12
        # Avoid division by zero
        return p
    end
    # get orthonormal unit vectors spanning the plane
    vh = v.v/theta
    # now just rotate in that plane
    q = cos(theta)*p.p + sin(theta)*vh
    projectToSphere(q) # just to be sure we stay normalized
end

function logmap(p::NSpherePoint,q::NSpherePoint)
    #print("Entering sphere log map\n")
    c = Base.dot(p.p,q.p) # dot product is cosine of angle (since both unit norm)
    t = acos(c)
    v = (q.p-c*p.p) # component of q orthogonal to p
    if Base.norm(v) < 1e-10
        return NSphereTangentBundlePoint(p,zeros(size(v))) # avoid divide by zero
    end
    v = v/Base.norm(v)
    # We actually wanna always return a tangent bundle type here
    NSphereTangentBundlePoint(p,v*t)
end

function partrans(p::NSpherePoint,v::NSphereTangentVector,X::NSphereTangentVector)
    theta = Base.norm(v.v)

    if theta < 1e-12
       # Avoid division by zero
       return X
    end

    c = cos(theta)
    s = sin(theta)

    # get orthonormal unit vectors spanning the plane
    ph = p.p/Base.norm(p.p)
    vh = v.v/Base.norm(v.v)

    # find components along p and v of X
    Xp = Base.dot(X.v,ph)
    Xv = Base.dot(X.v,vh)
    Xo = X.v - Xp*ph - Xv*vh

    # orthogonal stays put and we convert the tangential back to n+1-vec
    Xn = Xo + (c*Xp - s*Xv)*ph + (s*Xp + c*Xv)*vh

    # Also move the point along...
    pn = projectToSphere(c * ph + s * vh) # this is exponential map

    projectTangent(pn,Xn)
end

function rotMatrix(p::NSpherePoint,v::NSphereTangentVector)
    theta = Base.norm(v.v)

    # identity
    I = eye(length(v.v[:]))

    if theta < 1e-12
       # Avoid division by zero
       return I
    end

    c = cos(theta)
    s = sin(theta)

    # get orthonormal unit vectors spanning the plane
    ph = p.p/Base.norm(p.p)
    vh = v.v/Base.norm(v.v)

    # orthogonal stays put and we convert the tangential back to n+1-vec
    # Fix the orthogonal part, rotate the others
    # This is the full rotation matrix
    # Note this can be used as the parallel transport matrix also
    I - ph*ph' - vh*vh' + ph*(c*ph'-s*vh') + vh*(s*ph'+c*vh')
end

function partransMatrix(p::NSpherePoint,v::NSphereTangentVector)
    return rotMatrix(p,v)
    theta = Base.norm(v.v)

    # identity
    I = eye(length(v.v[:]))

    if theta < 1e-12
       # Avoid division by zero
       return I
    end

    c = cos(theta)
    s = sin(theta)

    # get orthonormal unit vectors spanning the plane
    ph = p.p/Base.norm(p.p)
    vh = v.v/Base.norm(v.v)

    # Now rotate in the plane (already have xy coords ph,vh)
    #R = [c -s; s c]

    # orthogonal stays put and we convert the tangential back to n+1-vec
    #(I - [ph vh]*[ph vh]') + [ph vh]*R*[ph vh]'
    # Fix the orthogonal part, rotate the others
    # This is the full rotation matrix
    #I - ph*ph' - vh*vh' + ph*(c*ph'-s*vh') + vh*(s*ph'+c*vh')
    # don't need the whole rotation matrix for partrans (note this is in Ring&Wirth2000)
    I + (c-1)*vh*vh' - s*ph*vh'
end

function curvtensor(X::NSphereTangentVector,Y::NSphereTangentVector,Z::NSphereTangentVector)
    # standard constant curvature formula
    dot(X,Z)*Y - dot(Y,Z)*X
end

