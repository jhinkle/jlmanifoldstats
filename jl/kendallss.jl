require("nsphere.jl")

# NOTE: what I refer to here as KendallShape is actually a "preshape", so care
# must be taken to do horizontal projection, yada yada

type KendallShape
    p::Array
    function KendallShape(p::Array)
        if Base.norm(mean(p,2)) > 1e-8
            error("Kendall shape must be zero-centered")
        end
        if abs(Base.norm(p[:]) - 1) > 1e-8
            error("Kendall shape must have unit norm")
        end
        new(p)
    end
end
function KendallShape(p::Vector)
    np = convert(Int,round(length(p)/2))
    KendallShape(reshape(p,(2,np)))
end
function KendallShape(p::NSpherePoint)
    KendallShape(p.p)
end

function makeKendallShape(p0)
    # center
    p = bsxfun(-,p0,mean(p0,2)) # subtract out the centroid

    # scale
    p /= Base.norm(p[:])

    KendallShape(p)
end

type KendallTangentVector
    v::Array
    function KendallTangentVector(u::Array)
        #if norm(mean(u,2)) > 1e-7
            #error("Kendall tangent vector must have zero mean")
        #end
        new(u)
    end
end
function KendallTangentVector(v::Vector)
    np = convert(Int,round(length(v)/2))
    KendallTangentVector(reshape(v,(2,np)))
end
function KendallTangentVector(v::NSphereTangentVector)
    KendallTangentVector(v.v)
end

function normSquared(v::KendallTangentVector)
    vec = v.v[:]
    Base.dot(vec,vec)
end

function +(u::KendallTangentVector,v::KendallTangentVector)
    KendallTangentVector(u.v+v.v)
end
function -(u::KendallTangentVector,v::KendallTangentVector)
    KendallTangentVector(u.v-v.v)
end
function *(a::Number,v::KendallTangentVector)
    KendallTangentVector(a*v.v)
end
function *(v::KendallTangentVector,a::Number)
    a*v
end
function /(v::KendallTangentVector,a::Number)
    KendallTangentVector(v.v/a)
end

type KendallTangentBundlePoint
    p::KendallShape
    v::KendallTangentVector
    function KendallTangentBundlePoint(p::KendallShape,v::KendallTangentVector)
        # TODO: Check that p and v are orthogonal
        new(p,v) 
    end
    KendallTangentBundlePoint(p::Array,v::Array) = new(KendallShape(p),KendallTangentVector(v))
    KendallTangentBundlePoint(p::KendallShape) = new(p,KendallTangentVector(zeros(size(p))))
    function KendallTangentBundlePoint(p::NSpherePoint,v::NSphereTangentVector)
        new(KendallShape(p.p),KendallTangentVector(v.v))
    end
    function KendallTangentBundlePoint(pv::NSphereTangentBundlePoint)
        new(KendallShape(pv.p.p),KendallTangentVector(pv.v.v))
    end
end
KendallTangentVector(pv::KendallTangentBundlePoint) = KendallTangentVector(pv.v.v)

function *(a::Number,pv::KendallTangentBundlePoint)
    KendallTangentBundlePoint(pv.p,a*pv.v)
end
function *(pv::KendallTangentBundlePoint,a::Number)
    a*pv
end
function /(pv::KendallTangentBundlePoint,a::Number)
    KendallTangentBundlePoint(pv.p,pv.v/a)
end

function dot(a::KendallTangentVector,b::KendallTangentVector)
    Base.dot(a.v[:],b.v[:])
end
function norm(a::KendallTangentVector)
    Base.norm(a.v)
end
function normSquared(a::KendallTangentBundlePoint)
    normSquared(a.v)
end

function distance(a::KendallShape,b::KendallShape)
    acos(a.p'*b.p) # can compute this without using log map for Kendall
end

function expmap(p::KendallShape,v::KendallTangentVector)
    psph = NSpherePoint(p.p[:])
    vsph = NSphereTangentVector(v.v[:])
    KendallShape(expmap(psph,vsph)) # do sphere exponential map
end
function expmap(pv::KendallTangentBundlePoint)
    expmap(pv.p,pv.v)
end

function alignShapes(p::KendallShape,q::KendallShape)
    # Procrustes align preshapes
    (U,S,V) = svd(p.p*q.p')

    # get R matrix
    R = U*V'

    # apply R
    KendallShape(R*q.p)
end

function logmap(p::KendallShape,q::KendallShape)
    qal = alignShapes(p,q)
    psph = NSpherePoint(p.p[:])
    qsph = NSpherePoint(qal.p[:])
    lsph = logmap(psph,qsph) # N-sphere log map
    v = reshape(lsph.v.v,size(p.p)) # uncentered velocity on big sphere
    v = bsxfun(-,v,mean(v,2))# project to zero mean
    KendallTangentBundlePoint(p,KendallTangentVector(v))
end

function complexStructure(X::KendallTangentVector)
    # the almost complex structure J from O'Neill 1966

    # it's equivalent to writing R^2d as C^d and multiplying by the complex unit i
    KendallTangentVector([-X.v[2,:];X.v[1,:]])
end

function partrans(p::KendallShape,v::KendallTangentVector,w::KendallTangentVector)
    # this is just the sphere version
    ps = NSpherePoint(p.p[:])
    vs = NSphereTangentVector(v.v[:])
    ws = NSphereTangentVector(w.v[:])
    h = reshape(partrans(ps,vs,ws).v,size(p.p))
    KendallTangentVector(bsxfun(-,h,mean(h,2)))
    #KendallTangentVector(h)
end

function horzproj(p::KendallShape, v::KendallTangentVector)
    # Project against J(x)                                                                                     
    w1 = [-p.p[2,:];p.p[1,:]] #this is JN (p.p is the normal direction)
    w1 = w1/Base.norm(w1[:]); # unit vector in vertical direction (should be 1)
    h = v.v - w1*Base.dot(v.v[:],w1[:])
    #KendallTangentVector(h)
    KendallTangentVector(bsxfun(-,h,mean(h,2)))
end

function horzprojMatrix(p::KendallShape)
    # matrix version of horzproj
    w1 = [-p.p[2,:];p.p[1,:]] #this is JN (p.p is the normal direction)
    w1 = w1/Base.norm(w1[:]); # unit vector in vertical direction (should be 1)
    
    eye(length(w1[:])) - w1[:]*w1[:]'
end

function curvtensor(X::KendallTangentVector,Y::KendallTangentVector,Z::KendallTangentVector)
    # curvature tensor on shape space

    # compute sphere curvature
    Xs = NSphereTangentVector(X.v[:])
    Ys = NSphereTangentVector(Y.v[:])
    Zs = NSphereTangentVector(Z.v[:])
    Rs = KendallTangentVector(curvtensor(Xs,Ys,Zs))

    # need some J(X) computations
    JX = complexStructure(X)
    JY = complexStructure(Y)
    JZ = complexStructure(Z)

    # adjust via O'Neill
    Rs + 2*dot(X,JY)*JZ - dot(Y,JZ)*JX - dot(Z,JX)*JY
end

function projectTangent(p::KendallShape,v::KendallTangentVector)
    # remove mean
    vp = bsxfun(-,v.v,mean(v.v,2))
    # make tangent to sphere at p
    vp = KendallTangentVector(reshape(vp[:] - p.p[:]*Base.dot(p.p[:],vp[:]),size(v.v)))
    # project to horizontal (optional)
    horzproj(p,vp)
end

function partransMatrix(p::KendallShape,v::KendallTangentVector)
    # return a big matrix representing parallel transport of a linearized vector
    # w along the geodesic determined by (p,v)

    # this will be just the sphere version (TODO: need to put horzproj matrix here too?)
    horzprojMatrix(expmap(p,v))*partransMatrix(NSpherePoint(p.p[:]),NSphereTangentVector(v.v[:]))*horzprojMatrix(p)
    #partransMatrix(NSpherePoint(p.p[:]),NSphereTangentVector(v.v[:]))
end
