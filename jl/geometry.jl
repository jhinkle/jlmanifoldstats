include("metricspace.jl")

abstract Manifold
abstract FinslerManifold <: Manifold
abstract RiemannianManifold <: FinslerManifold

abstract ManifoldPoint{M<:Manifold}
abstract TangentVector{M<:Manifold}
abstract TangentBundlePoint{M<:Manifold}

# tangent vector stuff
function +(a::TangentVector,b::TangentVector)
    p = a
    p.v += b.v
    p
end
function -(a::TangentVector,b::TangentVector)
    p = a
    p.v += b.v
    p
end
function *(a::TangentVector,b::Number)
    p = a
    p.v *= b
    p
end
function *(b::Number,a::TangentVector)
    a*b
end
function /(a::TangentVector,b::Number)
    p = a
    p.v /= b
    p
end

# tangent bundle point stuff
function norm{M<:FinslerManifold}(a::TangentBundlePoint{M})
    norm(a.v)
end
function dot{M<:RiemannianManifold}(pva::TangentBundlePoint{M},pvb::TangentBundlePoint{M})
    if (abs(pva.p-pvb.p) > 10*eps())
        error("To subtract tangent bundle points, base point must match")
    end
    dot(pva.v,pvb.v)
end
function +{T<:TangentBundlePoint}(pva::T,pvb::T)
    if (norm(pva.p.p-pvb.p.p) > 10*eps())
        error("To add tangent bundle points, base point must match")
    end
    p = pva
    pva.v += pvb.v
    p
end
function -{T<:TangentBundlePoint}(pva::T,pvb::T)
    if (abs(pva.p-pvb.p) > 10*eps())
        error("To subtract tangent bundle points, base point must match")
    end
    p = pva
    pva.v -= pvb.v
    p
end
function *(pv::TangentBundlePoint,a::Number)
    pva = pv
    pva.v *= a
    pva
end
function *(a::Number,pv::TangentBundlePoint)
    pv*a
end
function /(pv::TangentBundlePoint,a::Number)
    pva = pv
    pva.v /= a
    pva
end


# Just a common idiom for exponential map For many spaces closed form
# exponential maps are not available, in which case only expmapVelInf needs to be implemented
# Note that infinitesimal versions of parallel translation and exponential map
# are always required, but default to the long-distance versions
# TODO: manifold Leapfrog version (needs split expmapInf and partransInf)
function expmapVel{M<:RiemannianManifold}(pv0::TangentBundlePoint{M})
    pv = pv0
    for iter=1:Niter
        pv = expmapVelInf(pv,dt)
    end
    pv
end
function expmap{M<:RiemannianManifold}(pv0::TangentBundlePoint{M})
    expmapVel(pv0).p
end


# this is a generic Jacobi field integrator that just needs curvature and parallel transport defined
function integrateJacobiField{M<:RiemannianManifold}(p0::ManifoldPoint{M},v0::TangentVector{M},J0::TangentVector{M},Nsteps,stepsize)
    # just integrate Jacobi equation along geodesic using standard formula
    p = p0
    v = v0
    J = J0
    Jdot = newTangentVector(M)
    for step=1:Nsteps
        # increment Jdot by curvature
        Jdot += dt*curvtensor(J,v,v)

        # parallel translate J and Jdot

        # exponential map (infinitesimal version) to get next point (with velocity)
    end
    # return a pair (p,J)
    newTangentBundlePoint(M,p,J)
end


# the following is a general iterative log map that uses an extrinsic distance
# function and minimizes it using an intrinsic method on the manifold (Jacobi
# fields)
function iterativeLogMap{M<:RiemannianManifold}(p::ManifoldPoint{M},v0::TangentVector{M},q::ManifoldPoint{M}, Niter,alphaGD, Nsteps,stepsize)
    v = v0
    for iter=1:Niter
        # exponentiate
        ep,vp = expmapVel(p,v)

        # get extrinsic gradient (always just use vector difference)
        g = q.p-ep.p

        # project to be tangent (this only makes sense for embedded manifolds)
        # projectTangent should throw an error if it doesn't make sense for M
        J = projectTangent(ep,g)

        # integrate Jacobi field backwards to zero
        J0 = integrateJacobiField(J,ep,-vp,Nsteps,stepsize)

        # use Jacobi field for v update
        v -= alphaGD*J0
    end
    newTangentBundlePoint(M,p,v)
end
function iterativeLogMap{M<:RiemannianManifold}(pv0::TangentBundlePoint{M},q::ManifoldPoint{M}, Niter,alphaGD, Nsteps,stepsize)
    iterativeLogMap(pv0.p,v0,q,Niter,alphaGD,Nsteps,stepsize)
end
function iterativeLogMap{M<:RiemannianManifold}(p::ManifoldPoint{M},q::ManifoldPoint{M})
    # come up with a (possibly crappy) estimate of log map, based on extrinsic stuff
    v = projectTangent(p,q.p-p.p)

    pv = newTangentBundlePoint{M}(p,v)
    # some default values for optimization and integration params
    iterativeLogMap(pv,q,100,.1,100,.01)
end
