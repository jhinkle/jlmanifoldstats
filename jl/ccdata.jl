function loadCCData()
    # load bookstein rat data, return a Vector of 2x8 Arrays, and the associated times
        
    #  Bookstein rat data
    dim = 2;
    D = readcsv("cc.csv")
    # we throw out the three incomplete datasets

    y = Array[]
    t = Real[]
    for i=1:size(D,1)
        # skip the rows whose first column match those in skips
        push!(t,D[i,2]) # actual time in years
        yi = [D[i,3:2:end]; D[i,4:2:end]]
        push!(y,yi)
    end

    (y,t)
end
