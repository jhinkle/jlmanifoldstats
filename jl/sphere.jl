include("geometry.jl")

# TODO: Parametrize for n-sphere, but default to 2-sphere
abstract Sphere <: RiemannianManifold

type SpherePoint <: ManifoldPoint{Sphere}
    p::Vector
    SpherePoint(p::Array) = abs(norm(p)-1) > 10*eps() ? error("Sphere point needs to have unit norm") : new(squeeze(p))
end
newManifoldPoint(::Type{Sphere},p) = SpherePoint(p)

type SphereTangentVector <: TangentVector{Sphere}
    v::Vector
    SphereTangentVector(u::Array) = new(squeeze(u))
    SphereTangentVector() = new([0,0,0])
end
newTangentVector(::Type{Sphere},u) = SphereTangentVector(u)
newTangentVector(::Type{Sphere}) = SphereTangentVector()

type SphereTangentBundlePoint <: TangentBundlePoint{Sphere}
    p::SpherePoint
    v::SphereTangentVector
    function SphereTangentBundlePoint(p::SpherePoint,v::SphereTangentVector)
        if abs(dot(p.p,v.v)) > 10*eps()
            error("Sphere tangent vector ($(v.v)) must be orthogonal to point ($(p.p)): dot product is $(dot(p.p,v.v))\n")
        end
        new(p,v) 
    end
    SphereTangentBundlePoint(p::Array,v::Array) = new(SpherePoint(p),SphereTangentVector(v))
    SphereTangentBundlePoint(p::SpherePoint) = new(p,SphereTangentVector())
end
SphereTangentVector(pv::SphereTangentBundlePoint) = SphereTangentVector(pv.v.v)
newTangentBundlePoint(::Type{Sphere},p,v) = SphereTangentBundlePoint(p,v)
newTangentBundlePoint(::Type{Sphere},p) = SphereTangentBundlePoint(p)

function dot{T<:SphereTangentVector}(a::T,b::T)
    dot(a.v,b.v)
end
function norm{T<:SphereTangentVector}(a::T)
    norm(a.v)
end


function distance{M<:Sphere}(a::ManifoldPoint{M},b::ManifoldPoint{M})
    acos(a.p'*b.p) # can compute this without using log map for Sphere
end


function sphereExpMap(p::Vector,v::Vector)
    # find angle
    theta = norm(v)
    if theta < 1e-12
        # Avoid division by zero
        return SpherePoint(p)
    end
    # get orthonormal unit vectors spanning the plane
    ph = p/norm(p)
    vh = v/norm(v)
    # now just rotate in that plane
    q = cos(theta)*ph + sin(theta)*vh
    SpherePoint(q/norm(q))
end
function expmap{M<:Sphere}(p::ManifoldPoint{M},v::TangentVector{M})
    sphereExpMap(p.p,v.v)
end
function expmap{M<:Sphere}(pv::TangentBundlePoint{M})
    expmap(pv.p,pv.v)
end


# TODO: expmapVel (and alias the Inf version)


function sphereLogMap(p::Vector,q::Vector)
    #print("Entering sphere log map\n")
    c = dot(p,q) # dot product is cosine of angle (since both unit norm)
    t = acos(c)
    v = (q-dot(p,q)*p)
    if norm(v) < 1e-7
        return SphereTangentBundlePoint(p,[0,0,0]) # avoid divide by zero
    end
    v = v/norm(v)
    #print("log map is $(v*t) and type is $(typeof(v*t))\n")
    #v*t # actual tangent vector
    # We actually wanna always return a tangent bundle type here
    #TangentBundlePoint{Sphere}(ManifoldPoint{Sphere}(p),TangentVector{Sphere}(v*t))
    SphereTangentBundlePoint(p,v*t)
end
function logmap{M<:Sphere}(p::ManifoldPoint{M},q::ManifoldPoint{M})
    sphereLogMap(p.p,q.p)
end


function partrans{M<:Sphere}(p::ManifoldPoint{M},v::TangentVector{M},u::TangentVector{M})
    # parallel translation is just like exponential map

    # find angle
    theta = norm(v);

    if theta < 1e-12
       # Avoid division by zero
       return u
    end

    c = cos(theta);
    s = sin(theta);

    # get orthonormal unit vectors spanning the plane
    ph = p.p/norm(p.p);
    vh = v/norm(v);

    # find components along p and v of X
    utang = [ph vh]'*u;
    uorth = u - [ph vh]*utang;

    # Now rotate in the plane (already have xy coords ph,vh)
    R = [c -s; s c];
    utangrot = R*utang;

    # orthogonal stays put and we convert the tangential back to n+1-vec
    un = uorth + [ph vh]*utangrot;

    # Also move the point along...
    pn = c * p + s * vh;

    # Now make sure the vector remains tangent
    return un - (un'*un)*pn;
end


function curvtensor{M<:Sphere}(p::ManifoldPoint{M},u::TangentVector{M},v::TangentVector{M},w::TangentVector{M})
    # Apply the constant curvature formula
    newTangentBundlePoint(M,p,v*dot(u,w) - u*dot(v,w))
end
