include("geometry.jl")

# TODO: Parametrize for n-sphere, but default to 2-sphere
abstract Paraboloid{a,b,c} <: RiemannianManifold

type ParaboloidPoint{a,b,c} <: ManifoldPoint{Paraboloid{a,b,c}}
    p::Vector
    function ParaboloidPoint(p)
        # check that point lies on the paraboloid
        if abs(p[1]^2/a^2+p[2]^2/b^2-p[3]/c) > 100*eps()
            error("Tried to initialize paraboloid point with point not
            on the paraboloid!")
        end
        new(squeeze(p))
    end
end
function liftToParaboloid(p::Array,a,b,c)
    x = p[1]
    y = p[2]
    z = c*x^2/a^2 + c*y^2/b^2
    println("Lifted point $p to $([x,y,z])")
    [x,y,z]
end
function ParaboloidPoint{T,a,b,c}(p::Array{T,2})
    # given a point in the plane, just compute the 3D point and
    # initialize with that
    ParaboloidPoint{a,b,c}(liftToParaboloid(p,a,b,c))
end
newManifoldPoint{a,b,c}(::Type{Paraboloid{a,b,c}},p) = ParaboloidPoint{a,b,c}(p)


type ParaboloidTangentVector{a,b,c} <: TangentVector{Paraboloid{a,b,c}}
    v::Vector
    ParaboloidTangentVector(u::Array) = new(squeeze(u))
    ParaboloidTangentVector() = new([0,0,0])
end
newTangentVector{a,b,c}(::Type{Paraboloid{a,b,c}},u) = ParaboloidTangentVector{a,b,c}(u)
newTangentVector{a,b,c}(::Type{Paraboloid{a,b,c}}) = ParaboloidTangentVector{a,b,c}()


function dot{T<:ParaboloidTangentVector}(a::T,b::T)
    dot(a.v,b.v)
end
function norm{T<:ParaboloidTangentVector}(a::T)
    norm(a.v)
end


type ParaboloidTangentBundlePoint{a,b,c} <: TangentBundlePoint{Paraboloid{a,b,c}}
    p::ParaboloidPoint{a,b,c}
    v::ParaboloidTangentVector{a,b,c}
    function ParaboloidTangentBundlePoint(p::ParaboloidPoint,v::ParaboloidTangentVector)
        # we have two spanning vectors e_1,e_2 given by the following
        e1 = [1,0,2*c*p.p[1]/a^2]
        e2 = [0,1,2*c*p.p[2]/b^2]
        # now let's see if this point is tangent to the e1,e2-plane
        n = cross(e1,e2)
        n /= norm(n)

        # TODO: an arbitrarily chosen level of orthogonality.  Best would be an isOrthog() function
        if abs(dot(n,v.v)) > 1e-5*norm(v.v)
            error("Paraboloid tangent vector ($(v.v)) must be tangent at point ($(p.p))")
        end
        new(p,v) 
    end
    ParaboloidTangentBundlePoint(p::Array,v::Array) = new(ParaboloidPoint{a,b,c}(p),ParaboloidTangentVector{a,b,c}(v))
    ParaboloidTangentBundlePoint(p::ParaboloidPoint{a,b,c}) = new(p,ParaboloidTangentVector{a,b,c}())
end
ParaboloidTangentVector{a,b,c}(pv::ParaboloidTangentBundlePoint{a,b,c}) = ParaboloidTangentVector{a,b,c}(pv.v.v)
newTangentBundlePoint{a,b,c}(::Type{Paraboloid{a,b,c}},p,v) = ParaboloidTangentBundlePoint{a,b,c}(p,v)
newTangentBundlePoint{a,b,c}(::Type{Paraboloid{a,b,c}},p) = ParaboloidTangentBundlePoint{a,b,c}(p)


function paraboloidExpMapIntegrate(p,v,a,b,c)
    # this function solves the geodesic ODE in the projected plane

    #println("Integrating expmap on parabola $a,$b,$c from $p in direction $v")

    # Always do same number of steps (TODO: how to pass parameters to this?)
    Nsteps = 1000
    dt = 1/Nsteps
    
    #println("Using $Nsteps steps and step size of $dt")

    # start by setting up notation to match notes with g(t) the curve
    g = p
    gd = v

    #print("Initial conditions g=$g and gd=$gd\n")

    for step=1:Nsteps
        A = [1+4*c^2*g[1]^2/a^4     4*c^2*g[1]*g[2]/a^2/b^2;
         4*c^2*g[1]*g[2]/a^2/b^2       1+4*c^2*g[2]^2/b^4]
        n = norm(gd./[a,b])^2
        B = (-4*c^2*n)*[g[1]/a^2,g[2]/b^2]
        #println("Solving A=$A B=$B")
        gdd = A\B
        #println("Got gdd=$gdd")
        #println("$g")

        # euler steps
        g += dt*gd
        gd += dt*gdd
    end
    g
end
function paraboloidExpMap(p,v,a,b,c)
    # solve the geodesic equation in 2D then just find the z point after
    # the fact
    liftToParaboloid(paraboloidExpMapIntegrate(p[1:2],v[1:2],a,b,c),a,b,c)
end
function expmap{a,b,c}(p::ManifoldPoint{Paraboloid{a,b,c}},v::TangentVector{Paraboloid{a,b,c}})
    newManifoldPoint(Paraboloid{a,b,c},paraboloidExpMap(p.p,v.v,a,b,c))
end
function expmap{M<:Paraboloid}(pv::TangentBundlePoint{M})
    expmap(pv.p,pv.v)
end


function partrans{a,b,c}(p::ManifoldPoint{Paraboloid{a,b,c}},v::TangentVector{Paraboloid{a,b,c}},w::TangentVector{Paraboloid{a,b,c}})
    # TODO: throw a warning if |w| is too big

    # this is an infinitesimal parallel translation
end


function curvtensor{a,b,c}(p::ManifoldPoint{Paraboloid{a,b,c}},u::TangentVector{Paraboloid{a,b,c}},v::TangentVector{Paraboloid{a,b,c}},w::TangentVector{Paraboloid{a,b,c}}) 
    # compute curvature and just return a tangent vector
end

function integrateJacobiField()
    # just integrate Jacobi equation using standard formula
end


# use the generic iterative log map code, needs Jacobi fields implemented
function logmap{a,b,c}(p::ManifoldPoint{Paraboloid{a,b,c}},q::ManifoldPoint{Paraboloid{a,b,c}})
    iterativeLogMap(p.p,q.p)
end


a=1
b=1
c=1
gv = newTangentBundlePoint(Paraboloid{a,b,c},[0,0,0],[1,0,0])
ge = expmap(gv)
println("gv=$gv Exp(gv)=$ge")

l = logmap(gv.p,ge)
println("Log map is l")


#type SphereTangentVector <: TangentVector{Sphere}
    #v::Vector
    #SphereTangentVector(u::Array) = new(squeeze(u))
    #SphereTangentVector() = new([0,0,0])
#end
#newTangentVector(::Type{Sphere},u) = SphereTangentVector(u)
#newTangentVector(::Type{Sphere}) = SphereTangentVector()

#type SphereTangentBundlePoint <: TangentBundlePoint{Sphere}
    #p::SpherePoint
    #v::SphereTangentVector
    #function SphereTangentBundlePoint(p::SpherePoint,v::SphereTangentVector)
        #if abs(dot(p.p,v.v)) > 10*eps()
            #error("Sphere tangent vector ($(v.v)) must be orthogonal to point ($(p.p)): dot product is $(dot(p.p,v.v))")
        #end
        #new(p,v) 
    #end
    #SphereTangentBundlePoint(p::Array,v::Array) = new(SpherePoint(p),SphereTangentVector(v))
    #SphereTangentBundlePoint(p::SpherePoint) = new(p,SphereTangentVector())
#end
#SphereTangentVector(pv::SphereTangentBundlePoint) = SphereTangentVector(pv.v.v)
#newTangentBundlePoint(::Type{Sphere},p,v) = SphereTangentBundlePoint(p,v)
#newTangentBundlePoint(::Type{Sphere},p) = SphereTangentBundlePoint(p)

#function dot{T<:SphereTangentVector}(a::T,b::T)
    #dot(a.v,b.v)
#end
#function norm{T<:SphereTangentVector}(a::T)
    #norm(a.v)
#end


#function distance{M<:Sphere}(a::ManifoldPoint{M},b::ManifoldPoint{M})
    #acos(a.p'*b.p) # can compute this without using log map for Sphere
#end


#function sphereExpMap(p::Vector,v::Vector)
    ## find angle
    #theta = norm(v)
    #if theta < 1e-12
        ## Avoid division by zero
        #return SpherePoint(p)
    #end
    ## get orthonormal unit vectors spanning the plane
    #ph = p/norm(p)
    #vh = v/norm(v)
    ## now just rotate in that plane
    #q = cos(theta)*ph + sin(theta)*vh
    #SpherePoint(q/norm(q))
#end
#function expmap{M<:Sphere}(p::ManifoldPoint{M},v::TangentVector{M})
    #sphereExpMap(p.p,v.v)
#end
#function expmap{M<:Sphere}(pv::TangentBundlePoint{M})
    #expmap(pv.p,pv.v)
#end


#function sphereLogMap(p::Vector,q::Vector)
    ##print("Entering sphere log map\n")
    #c = dot(p,q) # dot product is cosine of angle (since both unit norm)
    #t = acos(c)
    #v = (q-dot(p,q)*p)
    #if norm(v) < 1e-7
        #return SphereTangentBundlePoint(p,[0,0,0]) # avoid divide by zero
    #end
    #v = v/norm(v)
    ##print("log map is $(v*t) and type is $(typeof(v*t))\n")
    ##v*t # actual tangent vector
    ## We actually wanna always return a tangent bundle type here
    ##TangentBundlePoint{Sphere}(ManifoldPoint{Sphere}(p),TangentVector{Sphere}(v*t))
    #SphereTangentBundlePoint(p,v*t)
#end
#function logmap{M<:Sphere}(p::ManifoldPoint{M},q::ManifoldPoint{M})
    #sphereLogMap(p.p,q.p)
#end


#function partrans{M<:Sphere}(p::ManifoldPoint{M},v::TangentVector{M},u::TangentVector{M})
    ## parallel translation is just like exponential map

    ## find angle
    #theta = norm(v);

    #if theta < 1e-12
       ## Avoid division by zero
       #return u
    #end

    #c = cos(theta);
    #s = sin(theta);

    ## get orthonormal unit vectors spanning the plane
    #ph = p.p/norm(p.p);
    #vh = v/norm(v);

    ## find components along p and v of X
    #utang = [ph vh]'*u;
    #uorth = u - [ph vh]*utang;

    ## Now rotate in the plane (already have xy coords ph,vh)
    #R = [c -s; s c];
    #utangrot = R*utang;

    ## orthogonal stays put and we convert the tangential back to n+1-vec
    #un = uorth + [ph vh]*utangrot;

    ## Also move the point along...
    #pn = c * p + s * vh;

    ## Now make sure the vector remains tangent
    #return un - (un'*un)*pn;
#end


#function curvtensor{M<:Sphere}(p::ManifoldPoint{M},u::TangentVector{M},v::TangentVector{M},w::TangentVector{M})
    ## Apply the constant curvature formula
    #newTangentBundlePoint(M,p,v*dot(u,w) - u*dot(v,w))
#end
