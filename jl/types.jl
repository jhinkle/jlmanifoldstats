# set up a basic type heirarchy here
abstract MetricSpacePoint
abstract ManifoldPoint
abstract TangentVector
abstract CotangentVector
abstract TangentBundlePoint
abstract CotangentBundlePoint
abstract RiemannianManifoldPoint <: ManifoldPoint,MetricSpacePoint
abstract RiemannianTangentVector <: TangentVector

# tangent vector stuff
function norm(a::TangentVector)
    norm(a.v)
end
function dot(a::TangentVector,b::TangentVector)
    dot(a.v,b.v)
end
function +(a::TangentVector,b::TangentVector)
    p = a
    p.v += b.v
    p
end
function -(a::TangentVector,b::TangentVector)
    p = a
    p.v += b.v
    p
end
function *(a::TangentVector,b::Number)
    p = a
    p.v *= b
    p
end
function *(b::Number,a::TangentVector)
    a*b
end
function /(a::TangentVector,b::Number)
    p = a
    p.v /= b
    p
end

# tangent bundle point stuff
function norm(a::TangentBundlePoint)
    norm(a.v)
end
function dot(pva::TangentBundlePoint,pvb::TangentBundlePoint)
    if (abs(pva.p-pvb.p) > 10*eps())
        error("To subtract tangent bundle points, base point must match")
    end
    dot(pva.v,pvb.v)
end
function +(pva::TangentBundlePoint,pvb::TangentBundlePoint)
    if (norm(pva.p.p-pvb.p.p) > 10*eps())
        error("To add tangent bundle points, base point must match")
    end
    p = pva
    pva.v += pvb.v
    p
end
function -(pva::TangentBundlePoint,pvb::TangentBundlePoint)
    if (abs(pva.p-pvb.p) > 10*eps())
        error("To subtract tangent bundle points, base point must match")
    end
    p = pva
    pva.v -= pvb.v
    p
end
function *(pv::TangentBundlePoint,a::Number)
    p = pva
    pva.v *= a
    p
end
function *(a::Number,pv::TangentBundlePoint)
    pv*a
end
function /(pv::TangentBundlePoint,a::Number)
    p = pva
    pva.v /= a
    p
end
