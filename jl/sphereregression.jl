include("nsphere.jl")

function frechetMean(y::Vector{NSpherePoint},eps,Niter)
    println("Computing frechet mean...")

    # Use average in Euclidean space as initial estimate
    # this is actually very close to the actual mean usually
    m = projectToSphere(mean(map(s->s.p,y)))

    SSE = Float64[]
    for iter = 1:Niter
        # take logs
        l = map(s->logmap(m,s).v,y)
        # compute SSE
        SSEi = sum(map(x->dot(x,x),l))
        push(SSE, SSEi)
        # report so we can watch convergence
        println("SSE = $SSEi")
        # get the sum of the logs
        dm = sum(l)
        # exponentiate to take gradient step
        m = expmap(m,eps*dm)
    end

    println("Frechet variance = $(SSE[end])")
    (m,SSE[end])
end

function integratePolynomial(p0::NSpherePoint,v0::Vector{NSphereTangentVector},t::Vector)
    # Integrate the polynomial equation and output a vector of points
    k = length(v0)

    pt = [deepcopy(p0)]
    vt = Vector{NSphereTangentVector}[deepcopy(v0)]

    p = deepcopy(p0)
    v = deepcopy(v0)
    for i=1:(length(t)-1)
        dt = t[i+1]-t[i] # time step

        if k == 1
            # have closed form geodesics (note we could save some computation by
            # forming the rotation matrix here)
            push(pt,expmap(p0,(t[i+1]-t[1])*v0[1]))
            push(vt,[partrans(p0,(t[i+1]-t[1])*v0[1],v0[1])])
            continue
        end
        # TODO: RK4 partrans method using partrans matrix
        vnext = deepcopy(v)
        if true
            # Do the approximate closed form solution for t->t+dt
            for j=1:k-1
                for l=1:(k-j)
                    vnext[j] += (dt^l)*v[j+l]/factorial(l)
                end
            end
            # use average velocity over the interval for exponential
            vel = 0.5*(v[1]+vnext[1])
            pnext = expmap(p,dt*vel)
            vnext = map(v -> partrans(p,dt*vel,v),vnext)
        else # Euler
            vel = v[1]
            for j=1:k-1
                v[j] += dt*vi[j+1] # polynomial part
            end
            pnext = expmap(p,dt*vel)
            # geometric stuff
            vnext = map(v -> partrans(p,dt*vel,v),v)
        end
        p = deepcopy(pnext)
        v = deepcopy(vnext)

        push(pt,p)
        push(vt,v)
    end
    (pt,vt)
end

function integrateAdjointEquations(t::Vector,
                                    pt::Vector{NSpherePoint},
                                    vt::Vector{Vector{NSphereTangentVector}},
                                    y::Vector{NSpherePoint},
                                    yind::Vector)
    k = length(vt[1])
    # initialize adjoint variables to zero
    l = repmat([NSphereTangentVector(zeros(size(y[1].p)))],k+1,1)
    SSE = 0.0

    I = eye(3)
    if k == 1
        # have closed form geodesic Jacobi fields
        theta = dot(vt[1][1],vt[1][1])
        for j = 1:length(yind)
            resid = logmap(pt[yind[j]],y[j]).v
            SSE += dot(resid,resid)

            # integrate the closed-form Jacobi equation (see my notes on ReducedJacobiFields)
            # lambda just rotates
            dt = t[yind[j]]-t[1] # delta t (this is not infinitesimal)
            l0 = projectTangent(pt[1],partrans(pt[yind[j]],(-dt)*vt[yind[j]][1],(-1)*resid))
            l[1] -= l0
            # get the velocity variation (this part's trickier)
            if abs(theta) < 1e-10
                # avoid divide by zero
                l[2] -= dt*l0
            else
                k = vt[1][1].v/theta
                l[2] -= projectTangent(pt[1],dt*l0.v - (cos(dt*theta)-1)/theta*cross(k,l0.v) -
                    (dt-sin(dt*theta)/theta)*cross(k,cross(k,l0.v)))
            end
        end

        return (l,SSE)
    end

    for i = length(t):-1:2
        dt = t[i]-t[i-1] # positive time step

        # check for data at t[i]
        for j = 1:length(y)
            if yind[j] == i
                resid = logmap(pt[i],y[j]).v
                l[1] += resid
                SSE += dot(resid,resid)
            end
        end

        # TODO: do polynomial higher-order terms here like we do for forward

        # add curvature parts
        for j = 1:k
            l[1] += dt*curvtensor(vt[i][j],l[j+1],vt[i][1])
        end

        # cascade parts
        for j=k+1:-1:2
            l[j] += dt*l[j-1]
        end

        # parallel transport backwards then horizontal project
        l = map(li -> partrans(pt[i],-dt*vt[i][1],li), l)
    end

    # check for data at t=0
    for j = 1:length(yind)
        if yind[j] == 1
            resid = logmap(pt[1],y[j]).v
            l[1] += resid
            SSE += dot(resid,resid)
        end
    end

    (l,SSE)
end

function takeStep(p00,v00,dxa,eps)
    # the "retraction" (cf Ring&Wirth or Qi etal2010)
    k = length(v00)

    # take the step
    v0 = map(i -> partrans(p00,eps*dxa[1], v00[i] + eps*dxa[i+1]),[1:k])
    p0 = expmap(p00,eps*dxa[1])

    (p0,v0)
end

function objectiveFunction(p00,v00,dxa,stepsize,t,y,yind)
    (p0,v0) = takeStep(p00,v00,dxa,stepsize)

    # integrate
    (pt,vit) = integratePolynomial(p0,v0,t)

    # compute SSE
    SSE = 0.0
    for j=1:length(yind)
        resid = logmap(pt[yind[j]],y[j]).v
        SSE += dot(resid,resid)
    end
    #SSE /= length(yind)

    (p0,v0,pt,vit,SSE)
end

function lineSearchArmijo(p00,v00,dxa,l,t,y,yind,SSE0,a0)
    # perform backtracking line search to satisfy Wolfe criteria

    k = length(v00)

    a = a0 # i know Qi says start with 1, but that's nuts sometimes
    # this is c1*(search direction)*(gradient)
    #c = (1e-4)*sum(i->dot(dxa[i].v[:],l[i].v[:]),[1:k+1])
    #c = -abs(c)
    c = 0

    growfactor = 2.0

    # integrate naive direction
    (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,growfactor*a,t,y,yind)
    #println("a*c=$(a*c)")
    if (SSE - SSE0 < a*c)# && (i < 50)
        # if twice current stepsize decreases energy
        i = 1
        while (SSE - SSE0 < a*c)# && (i < 50)
            #print("+")
            a = growfactor*a # it's ok to increase stepsize, since apparently it won't increase SSE
            (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,growfactor*a,t,y,yind)
            i += 1
        end
    end
    (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,a,t,y,yind)
    i = 1
    #if SSE - SSE0 >= .5*a*c
        # need to decrease the energy at least once
        while (SSE - SSE0 >= .5*a*c)  && (i < 20)
            #println("- ($(SSE-SSE0)) >= $(a*c) a=$a")
            a = 0.5*a
            (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,a,t,y,yind)
            i += 1
        end
    #end
    #lnew = 0
    (lnew,SSE) = integrateAdjointEquations(t,pt,vit,y,yind)
    # (strong) curvature condition
    # this slows things down but technically we need it to ensure positive hessian
    c2 = .1 # value of 0.9 suggested by wikipedia for quasi-Newton
    i = 0
    while (abs(sum(i->dot(dxa[i],lnew[i]),[1:k+1])) < c2*abs(sum(i->dot(dxa[i],l[i]),[1:k+1])))# && (i < 10)
        #println("/ $(abs(sum(i->dot(dxa[i],lnew[i]),[1:k+1]))) < $(c2*abs(sum(i->dot(dxa[i],l[i]),[1:k+1]))) a=$a ")
        a = 0.5*a
        (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,a,t,y,yind)
        i += 1
        (lnew,SSE) = integrateAdjointEquations(t,pt,vit,y,yind)
    end

    # return all the stuff we've computed (reuse integrated path)
    (a,p0,v0,pt,vit,lnew,SSE)
end

function polynomialRegression{T<:NSpherePoint,V<:NSphereTangentVector}( y::Vector{T},
                                                t::Vector,
                                                yind::Vector,
                                                p0initial::T,
                                                v0initial::Vector{V},
                                                Niter)
    polynomialRegression(y,t,yind,p0initial,v0initial,Niter,0)
end
function polynomialRegression{T<:NSpherePoint,V<:NSphereTangentVector}( y::Vector{T},
                                                t::Vector,
                                                yind::Vector,
                                                p0initial::T,
                                                v0initial::Vector{V},
                                                Niter,
                                                bailUnder)
    # run polynomial regression and return a tuple of initial conditions, SSE
    k = length(v0initial)
    Np = length(yind)
    println("Fitting order $k polynomial to $Np sphere points...")

    # get initial estimate (remember pass-by-reference)
    p0 = deepcopy(p0initial)
    v0 = deepcopy(v0initial)
    k = length(v0)

    SSE = Float64[]

    # initial setup for quasi-Newton
    lprev = zeros((k+1)*length(p0.p[:]))
    sk = zeros((k+1)*length(p0.p[:]))
    #B = eye(2*Np*(k+1)) # approx Hessian
    I = eye(3*(k+1))
    H = I # approx inverse Hessian (previous iter, transported)
    T = I

    relstopcrit = 1e-4 # what percentage does gradient norm need to be to call it converged

    a = 0.01
    #doBFGS = true
    doBFGS = false
    doNCG = true # nonlinear conjugate gradient (Riemannian version)
    #doNCG = false # nonlinear conjugate gradient (Riemannian version)
    normlv0 = 0;
    p0i = [deepcopy(p0)]
    v0i = Vector{NSphereTangentVector}[deepcopy(v0)]
    for iter=1:Niter
        print("$iter/$Niter ")

        # integrate forward
        #println("v0=$v0")
        if iter == 1 || !(doBFGS || doNCG)
            (pt,vit) = integratePolynomial(p0,v0,t)
        end

        # integrate adjoint equations
        (l,SSEi) = integrateAdjointEquations(t,pt,vit,y,yind)
        #print("l=$l\n")

        if doBFGS
            # quasiNewton
            # vectorize l (all components)
            lv = reduce(vcat,map(li->li.v[:],l))

            yk = -(lv - lprev) # current gradient minus previous (remember l is neg. gradient)
            dotsy = dot(yk,sk)
            
            #Ti = inv(T'*T)*T' # Moore-Penrose pseudoinverse
            Ti = T'
            if dotsy <= 1e-9*(norm(sk)*norm(yk))^2 # avoid nan
                print("THT")
                #H = T*H*inv(T')
                H = T*H*Ti
            else
                # formula from Ring&Wirth
                #J = I - yk*sk'/dotsy
                #H = T*(J'*H*J + sk*sk'/dotsy)*Ti
                H = T*H*Ti
                H = H + (dot(sk,yk) + dot(yk,H*yk))*sk*sk'/dotsy^2 - (H*yk*sk'+sk*yk'*H)/dotsy
            end
            dx = (H*lv) # get search direction using quasi-Hessian
            # reset H occasionally
            if iter%5 == 0
                print("R ")
                #H = inv(.9*inv(H)+.1*I)
                H = I
            else
                print("  ")
            end

            #norml = norm(lv) # actual gradient norm
            norml = norm(dx) # norm of search direction
            if iter == 1
                normlv0 = norml
            elseif norml < relstopcrit*normlv0 # convergence test on gradient norm
                println("Reached stopping criterion early.  Rel. search dir norm=$(norml/normlv0)")
                break
            end

            #print("normgrad = $norml (started with $normlv0)")

            #if dot(dx,lv) < 0
                #dx = lv
            #end

            # reshape dx to a form we can use
            dxa = map(i -> NSphereTangentVector(dx[3*i+1:3*i+3]),[0:k])
            # project to tangent space at p0
            dxa = map(dxi -> projectTangent(p0,dxi),dxa)

            # do simple line search from Qi etal
            # NOTE: this updates p0,v0
            p0prev = p0
            (a,p0,v0,pt,vit) = lineSearchArmijo(deepcopy(p0),deepcopy(v0),dxa,l,t,y,yind,SSEi,a)
    
            # get parallel transport matrix
            T = kron(partransMatrix(p0prev,a*dxa[1]),eye(k+1))

            acc1 = partrans(p0prev,a*dxa[1],l[1])
            acc2 = T*lv

            sk = T*(a*dx)
            lprev = T*lv # keep this around for next time
            #B = B + yk*yk'/dot(yk,dx) - (B*dx)*(B*dx)'/dot(dx,B*dx)

        elseif doNCG
            # nonlinear conjugate gradient
            lv = reduce(vcat,map(li->li.v[:],l)) # get vectorized gradient

            dx = lv # initial search direction

            #norml = norm(lv) # actual gradient norm
            norml = norm(dx) # norm of search direction
            if iter == 1
                normlv0 = norml
            elseif norml < relstopcrit*normlv0 # convergence test on gradient norm
                print("Reached stopping criterion early.  Rel. search dir norm=$(norml/normlv0)")
                break
            else
                #print("r=$(norml/normlv0)")
            end

            # compute the adjusted search direction, s
            if iter == 1
                s = dx
            else
                # compute beta
                #beta = dot(dx,dx)/dot(dxprev,dxprev) # Fletcher-Reeves
                beta = max(0.0,dot(dx,dx-dxprev)/dot(dxprev,dxprev)) # Adjusted Polak-Ribiere
                if ((iter%10) == 1) || (iter > 2 && SSE[end-1] < SSEi)
                    beta = 0 # reset every so often
                    a = 0.01 # also reset step size in case it's shrunk to zero
                end
                s = dx + beta*sprev
            end

            # reshape search direction s to a form we can use
            sa = map(i -> projectTangent(p0,NSphereTangentVector(s[(3*i+1):3*(i+1)])),[0:k])

            # do simple line search from Qi etal
            # NOTE: this updates p0,v0
            p0prev = deepcopy(p0)
            (a,p0,v0,pt,vit) = lineSearchArmijo(deepcopy(p0),deepcopy(v0),sa,l,t,y,yind,SSEi,a)
            #print(" a=$a ")
    
            # get parallel transport matrix
            T = kron(partransMatrix(p0prev,a*sa[1]),eye(k+1))
            dxprev = T*dx
            sprev = T*s
        else 
            # update initial conditions and parallel transport them via p0 perturbation
            if false
                if iter > 1 && SSEi > SSE[end]
                    println("Reducing step size")
                    a *= 0.5 # simple backtrack without recomputing
                    p0 = p0i[end-1]
                    v0 = v0i[end-1]
                end
                (p0,v0) = takeStep(p0,v0,l,a)
                push(p0i,deepcopy(p0))
                push(v0i,deepcopy(v0))
            else
                # adaptive stepsize
                (a,p0,v0,pt,vit) = lineSearchArmijo(deepcopy(p0),deepcopy(v0),l,l,t,y,yind,SSEi,a)
                if iter > 1 && abs(SSE[end] - SSEi) < (1e-7)*SSE[end] # relative stopping criterion
                    println("Reached stopping criterion early.  Rel. search dir norm=$(abs(SSEi-SSE[end])/SSE[1])")
                    println("SSE=$SSEi")
                    push(SSE,SSEi)
                    break
                end
            end
        end

        println("SSE=$SSEi")
        push(SSE,SSEi)

        if SSEi < bailUnder
            break
        end
    end

    (p0,v0,SSE)
end

function computeR2pred{T<:NSpherePoint,V<:NSphereTangentVector}( y::Vector{T},
                                                t::Vector,
                                                yind::Vector,
                                                p0initial::T,
                                                v0initial::Vector{V},
                                                Niter)
    # compute a fit for each leave-one-out dataset and sum the missing
    # residuals.  i.e. the objective function for a leave-one-out experiment

    # find the mean
    (mu,SSEmu) = frechetMean(y,0.01,50)

    SSEpred = 0.0
    for i=1:length(yind)
        # remove the point
        ydel = deepcopy(y)
        yinddel = deepcopy(yind)
        del(ydel,i)
        del(yinddel,i)

        # do the regression
        (p0d,v0d,SSEdel) = polynomialRegression(ydel,t,yinddel,p0initial,v0initial,Niter)

        # shoot to get trajectory
        (pt,vt) = integratePolynomial(p0d,v0d,t)

        # compare the missing point
        resid = logmap(pt[yind[i]],y[i]).v
        SSEpred += normSquared(resid)
    end

    1 - SSEpred/SSEmu
end 
