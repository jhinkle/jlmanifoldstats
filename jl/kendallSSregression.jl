#include("kendallss.jl")

function frechetMean(y::Vector{KendallShape},eps,Niter)
    println("Computing frechet mean...")

    # Use average in Euclidean space as initial estimate
    m = makeKendallShape(mean(map(s->s.p,y)))

    SSE = Float64[]
    for iter = 1:Niter
        # take logs
        l = map(s->logmap(m,s).v,y)
        # compute SSE
        SSEi = sum(map(normSquared,l))
        push!(SSE, SSEi)
        # report so we can watch convergence
        println("SSE = $SSEi")
        # get the sum of the logs
        dm = sum(l)
        # exponentiate to take gradient step
        m = expmap(m,eps*dm)
    end

    println("Frechet variance = $(SSE[end])")
    (m,SSE[end])
end

function integratePolynomial(p0::KendallShape,v0::Vector{KendallTangentVector},t::Vector)
    # Integrate the polynomial equation and output a vector of points
    k = length(v0)

    pt = [deepcopy(p0)]
    vit = Vector{KendallTangentVector}[deepcopy(v0)]

    p = deepcopy(p0)
    vi = deepcopy(v0)
    for i=1:(length(t)-1)
        dt = t[i+1]-t[i] # time step

        # TODO: RK4 partrans method using partrans matrix
        vinext = deepcopy(vi)
        if true
            # Do the approximate closed form solution for t->t+dt
            for j=1:k-1
                for l=1:(k-j)
                    vinext[j] += (dt^l)*vi[j+l]/factorial(l)
                end
            end
            # use average velocity over the interval for exponential
            vel = 0.5*(vi[1]+vinext[1])
            pnext = expmap(p,dt*vel)
            vinext = map(v -> partrans(p,dt*vel,v),vinext)
        else # Euler
            vel = vi[1]
            for j=1:k-1
                vi[j] += dt*vi[j+1] # polynomial part
            end
            pnext = expmap(p,dt*vel)
            # geometric stuff
            vinext = map(v -> partrans(p,dt*vel,v),vi)
        end
        p = deepcopy(pnext)
        vi = deepcopy(vinext)

        push!(pt,p)
        push!(vit,vi)
    end
    (pt,vit)
end

function integrateAdjointEquations(t::Vector,
                                    pt::Vector{KendallShape},
                                    vit::Vector{Vector{KendallTangentVector}},
                                    y::Vector{KendallShape},
                                    yind::Vector)
    k = length(vit[1])
    # initialize adjoint variables to zero
    l = repmat([KendallTangentVector(zeros(size(y[1].p)))],k+1,1)
    SSE = 0.0

    for i = length(t):-1:2
        dt = t[i] - t[i-1] # positive time step

        # check for data at t[i]
        for j = 1:length(y)
            if yind[j] == i
                resid = logmap(pt[i],y[j]).v
                l[1] += resid
                SSE += normSquared(resid)
            end
        end

        # TODO: do polynomial higher-order terms here like we do for forward

        # add curvature parts
        for j = 1:k
            l[1] += dt*curvtensor(vit[i][j],l[j+1],vit[i][1])
        end

        # cascade parts
        for j=k+1:-1:2
            l[j] += dt*l[j-1]
        end

        # parallel transport backwards then horizontal project
        l = map(li -> horzproj(pt[i-1],partrans(pt[i],-dt*vit[i][1],li)), l)
    end

    # check for data at t=0
    for j = 1:length(yind)
        if yind[j] == 1
            resid = logmap(pt[1],y[j]).v
            l[1] += 2.0*resid
            SSE += normSquared(resid)
        end
    end

    (l,SSE)
end

function takeStep(p00,v00,dxa,eps)
    # the "retraction" (cf Ring&Wirth or Qi etal2010)
    k = length(v00)

    # take the step
    v0 = map(i -> partrans(p00,eps*dxa[1], v00[i] + eps*dxa[i+1]),[1:k])
    p0 = expmap(p00,eps*dxa[1])
    v0 = map(v->horzproj(p00,v),v0)

    (p0,v0)
end

function objectiveFunction(p00,v00,dxa,stepsize,t,y,yind)
    (p0,v0) = takeStep(p00,v00,dxa,stepsize)

    # integrate
    (pt,vit) = integratePolynomial(p0,v0,t)

    # compute SSE
    SSE = 0.0
    for j=1:length(yind)
        resid = logmap(pt[yind[j]],y[j]).v
        SSE += normSquared(resid)
    end
    #SSE /= length(yind)

    (p0,v0,pt,vit,SSE)
end

function lineSearchArmijo(p00in,v00in,dxa,l,t,y,yind,SSE0,a0,doExpansive)
    # perform backtracking line search to satisfy Wolfe criteria

    p00 = deepcopy(p00in)
    v00 = deepcopy(v00in)

    k = length(v00)

    a = a0 # i know Qi says start with 1, but that's nuts sometimes
    # this is c1*(search direction)*(gradient)
    #c = (1e-4)*sum(i->dot(dxa[i].v[:],l[i].v[:]),[1:k+1])
    #c = -abs(c)
    c = 0

    if doExpansive
        # integrate naive direction
        (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,2.0*a,t,y,yind)
        #println("a*c=$(a*c)")
        if (SSE - SSE0 < a*c)
            # if twice current stepsize decreases energy
            i = 1
            while (SSE - SSE0 < a*c) && (i <= 10)
                #print("+")
                a = 2.0*a # it's ok to increase stepsize, since apparently it won't increase SSE
                (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,2.0*a,t,y,yind)
                i += 1
            end
            (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,a,t,y,yind)
        end
    else
        (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,a,t,y,yind)
    end
    i = 1
    if SSE - SSE0 >= .5*a*c
        # need to decrease the energy at least once
        while (SSE - SSE0 >= .5*a*c)  && (i <= 50)
            #println("- ($(SSE-SSE0)) >= $(a*c) a=$a")
            a = 0.5*a
            (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,a,t,y,yind)
            i += 1
        end
    end
    (lnew,SSE) = integrateAdjointEquations(t,pt,vit,y,yind)
    # (strong) curvature condition
    # this slows things down but technically we need it to ensure positive hessian
    c2 = .1 # value of 0.9 suggested by wikipedia for quasi-Newton
    #c2 = .9 # value of 0.9 suggested by wikipedia for quasi-Newton
    # transport dxa along
    #dxat = map(d->partrans(p0,a*dxa[1],d),dxa)
    while (abs(sum(i->dot(dxa[i],lnew[i]),[1:k+1])) < c2*abs(sum(i->dot(dxa[i],l[i]),[1:k+1]))) && (i <= 1)
        #println("/ $(abs(sum(i->dot(dxa[i],lnew[i]),[1:k+1]))) < $(c2*abs(sum(i->dot(dxa[i],l[i]),[1:k+1]))) a=$a ")
        a = 0.5*a
        (p0,v0,pt,vit,SSE) = objectiveFunction(p00,v00,dxa,a,t,y,yind)
        i += 1
        (lnew,SSE) = integrateAdjointEquations(t,pt,vit,y,yind)
        dxat = map(d->partrans(p0,a*dxa[1],d),dxa)
    end
    #lnew = 0

    # return all the stuff we've computed (reuse integrated path)
    (a,p0,v0,pt,vit,lnew,SSE)
end

function polynomialRegression{T<:KendallShape,V<:KendallTangentVector}( y::Vector{T},
                                                t::Vector,
                                                yind::Vector,
                                                p0initial::T,
                                                v0initial::Vector{V},
                                                Niter,
                                                SSEmean)
    # run polynomial regression and return a tuple of initial conditions, SSE
    k = length(v0initial)
    Np = size(p0initial.p,2) # number of landmarks
    println("Fitting order $k polynomial to $Np landmarks...")

    # get initial estimate (remember pass-by-reference)
    p0 = deepcopy(p0initial)
    v0 = deepcopy(v0initial)
    k = length(v0)

    SSE = Float64[]

    # initial setup for quasi-Newton
    lprev = zeros((k+1)*length(p0.p[:]))
    sk = zeros((k+1)*length(p0.p[:]))
    #B = eye(2*Np*(k+1)) # approx Hessian
    I = eye(2*Np*(k+1))
    H = I # approx inverse Hessian (previous iter, transported)
    T = I

    stopIter = 100 # look back 10 iterations to check for convergence
    stopCrit = 1e-11 # _relative_ change needed to call it converged

    expandEvery = 5 # do _expansive_ search every N iterations

    a = 0.00001
    doBFGS = false
    doNCG = false # nonlinear conjugate gradient (Riemannian version)
    doStochHillClimb = false # stochastic hill climbing
    doFixedGD = false # stochastic hill climbing
    stochHCpert = 0.5 # sigma (relative to variation) for stochHC
    normlv0 = 0;
    for iter=1:Niter
        print("$iter/$Niter ")

        # integrate forward
        #println("v0=$v0")
        if iter == 1 || !(doBFGS || doNCG)
            (pt,vit) = integratePolynomial(p0,v0,t)
        end

        # integrate adjoint equations
        (l,SSEi) = integrateAdjointEquations(t,pt,vit,y,yind)
        #print("l=$l\n")

        for i=0:k
            l[i+1] *= 10.0^(-i)  # adjust to some "natural" scale
        end

        if doBFGS
            # quasiNewton
            # inspired by Ring&Wirht, as well as johnmyleswhite/Optim.jl

            # vectorize l (all components)
            lv = reduce(vcat,map(li->li.v[:],l))

            s = H*lv  # search dir neg gradient
            dphi0 = Base.dot(-lv,s)
            
            if dphi0 > 0 # reset if hessian points us in increasing direction
                print("R ")
                copy!(H, I)
                s = lv
                dphi0 = Base.dot(-lv, s)
            end
            clear!(lsr)
            push!(lsr, 0, f_x, dphi0)



            #else
                ## formula from Ring&Wirth
                ##J = I - yk*sk'/dotsy
                ##H = T*(J'*H*J + sk*sk'/dotsy)*Ti
                #H = T*H*Ti
                #H = H + (Base.dot(sk,yk) + Base.dot(yk,H*yk))*sk*sk'/dotsy^2 - (H*yk*sk'+sk*yk'*H)/dotsy
            #end
            dx = (H*lv) # get search direction using quasi-Hessian
            # reset H occasionally
            #if iter%3 == 0
                #print("R ")
                ##H = inv(.9*inv(H)+.1*I)
                #copy!(H, I)
            #else
                #print("  ")
            #end

            #norml = norm(lv) # actual gradient norm
            norml = Base.norm(dx) # norm of search direction
            #if iter == 1
            normlv0 = norml
            #elseif norml < relstopcrit*normlv0 # convergence test on gradient norm
                #println("Reached stopping criterion early.  Rel. search dir norm=$(norml/normlv0)")
                #break
            #end

            #print("normgrad = $norml (started with $normlv0)")

            #if dot(dx,lv) < 0
                #dx = lv
            #end

            # reshape dx to a form we can use
            dxa = map(i -> KendallTangentVector(reshape(dx[(2*Np*i+1):(2*Np*(i+1))],(2,Np))),[0:k])
            # project to tangent space at p0
            dxa = map(dxi -> projectTangent(p0,dxi),dxa)

            # do simple line search from Qi etal
            # NOTE: this updates p0,v0
            p0prev = p0
            (a,p0,v0,pt,vit) = lineSearchArmijo(deepcopy(p0),deepcopy(v0),dxa,l,t,y,yind,SSEi,a,true)
    
            # get parallel transport matrix
            T = kron(partransMatrix(p0prev,a*dxa[1]),eye(k+1))

            acc1 = partrans(p0prev,a*dxa[1],l[1])
            acc2 = T*lv

            sk = T*(a*dx)
            lprev = T*lv # keep this around for next time
            #B = B + yk*yk'/dot(yk,dx) - (B*dx)*(B*dx)'/dot(dx,B*dx)

        elseif doNCG
            # nonlinear conjugate gradient
            lv = reduce(vcat,map(li->li.v[:],l)) # get vectorized gradient

            dx = lv # initial search direction

            #norml = norm(lv) # actual gradient norm
            #norml = Base.norm(dx) # norm of search direction
            #if iter == 1
                #normlv0 = norml
            #end
            #elseif norml < relstopcrit*normlv0 # convergence test on gradient norm
                #println("Reached stopping criterion early.  Rel. search dir norm=$(norml/normlv0)")
                #break
            #end

            # compute the adjusted search direction, s
            if iter == 1
                s = dx
            else
                # compute beta
                #beta = dot(dx,dx)/dot(dxprev,dxprev) # Fletcher-Reeves
                beta = Base.dot(dx,dx-dxprev)/Base.dot(dxprev,dxprev) # Adjusted Polak-Ribiere
                if (iter%10) == 1 || (iter > 2 && SSE[end-1] < SSEi)
                    beta = 0.0 # reset every so often
                end
                s = dx + beta*sprev
            end

            # reshape search direction s to a form we can use
            sa = map(i -> KendallTangentVector(reshape(s[(2*Np*i+1):(2*Np*(i+1))],(2,Np))),[0:k])
            # project to tangent space at p0
            sa = map(xi -> projectTangent(p0,xi),sa)
            # reshape dx to a form we can use
            dxa = map(i -> KendallTangentVector(reshape(dx[(2*Np*i+1):(2*Np*(i+1))],(2,Np))),[0:k])
            # project to tangent space at p0
            dxa = map(dxi -> projectTangent(p0,dxi),dxa)

            # do simple line search from Qi etal
            # NOTE: this updates p0,v0
            p0prev = deepcopy(p0)
            (a,p0,v0,pt,vit) = lineSearchArmijo(deepcopy(p0),deepcopy(v0),sa,l,t,y,yind,SSEi,a,true)
    
            # get parallel transport matrix
            T = kron(partransMatrix(p0prev,a*sa[1]),eye(k+1))
            dxprev = T*dx
            sprev = T*s

            #dxprev = map(v->partrans(p0prev,a*sa[1],v),dxa)
            #sprev = map(v->partrans(p0prev,a*sa[1],v),sa)
            #dxprev = reduce(vcat,map(li->li.v[:],dxprev))
            #sprev = reduce(vcat,map(li->li.v[:],sprev))
        elseif doStochHillClimb
            for i=1:(k+1)
                # perturb gradient
                rv = KendallTangentVector(rand(size(l[i].v))) # get isotropic random variable
                rv = projectTangent(p0, rv)
                n = norm(l[i])
                l[i] = projectTangent(p0, l[i] + stochHCpert * n * rv)
            end
            #(p0,v0,pt,vit,SSEi) = objectiveFunction(deepcopy(p0),deepcopy(v0),l,a,t,y,yind)
            if iter % 10 == 0
                a = astoch
            end
            (a,p0,v0,pt,vit) = lineSearchArmijo(deepcopy(p0),deepcopy(v0),l,l,t,y,yind,SSEi,a, iter % expandEvery == 0)
        elseif doFixedGD
            if iter % 50 == 0
                (a,p0,v0,pt,vit) = lineSearchArmijo(deepcopy(p0),deepcopy(v0),l,l,t,y,yind,SSEi,a, true)
            else
                (p0,v0,pt,vit,SSEi) = objectiveFunction(deepcopy(p0),deepcopy(v0),l,a,t,y,yind)
            end
        else 
            # update initial conditions and parallel transport them via p0 perturbation
            (a,p0,v0,pt,vit) = lineSearchArmijo(deepcopy(p0),deepcopy(v0),l,l,t,y,yind,SSEi,a, iter % expandEvery == 0)
            #if iter > 100
                ## switch to hill climb
                #astoch = a
                #doStochHillClimb = true
            #end
            #if iter > 1 && (SSE[end] - SSEi) < 1e-5*SSE[1] # relative stopping criterion
                #println("Reached stopping criterion early.  Rel. search dir norm=$((SSEi-SSE[end])/SSE[1])")
            #end
        end

        println("SSE=$SSEi R^2=$(1-SSEi/SSEmean)")
        push!(SSE,SSEi)

        # check for stopping criteria
        if iter > stopIter && abs(SSE[iter-stopIter] - SSEi) < stopCrit*SSEi
            println("Reached stopping criterion early. Stopping.")
            break
        end
    end

    (p0,v0,SSE)
end

function computeR2pred{T<:KendallShape,V<:KendallTangentVector}( y::Vector{T},
                                                t::Vector,
                                                yind::Vector,
                                                p0initial::T,
                                                v0initial::Vector{V},
                                                Niter)
    # compute a fit for each leave-one-out dataset and sum the missing
    # residuals.  i.e. the objective function for a leave-one-out experiment

    # find the mean
    (mu,SSEmu) = frechetMean(y,0.01,50)

    SSEpred = 0.0
    for i=1:length(yind)
        # remove the point
        ydel = deepcopy(y)
        yinddel = deepcopy(yind)
        del(ydel,i)
        del(yinddel,i)

        # do the regression
        (p0d,v0d,SSEdel) = polynomialRegression(ydel,t,yinddel,p0initial,v0initial,Niter)

        # shoot to get trajectory
        (pt,vt) = integratePolynomial(p0d,v0d,t)

        # compare the missing point
        resid = logmap(pt[yind[i]],y[i]).v
        SSEpred += normSquared(resid)
    end

    1 - SSEpred/SSEmu
end 
