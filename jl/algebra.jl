include("geometry.jl")

abstract Group

abstract LieGroup <: Manifold,Group
abstract RiemannianLieGroup <: RiemannianManifold,LieGroup

# Note: Please prefix concrete types with LeftInv and RightInv, default
# will be biinv.  For instance SO3 is biinvariant (natural) metric
# but LeftInvSO3Point{A::Matrix} is a left-invariant version
abstract BiinvariantRiemannianLieGroup{A} <: RiemannianLieGroup
abstract LeftInvariantLieGroup{A} <: RiemannianLieGroup
abstract RightInvariantLieGroup{A} <: RiemannianLieGroup

abstract Algebra
abstract LieAlgebra{G<:LieGroup} <: Algebra

abstract GroupElement{G<:Group}
abstract AlgebraElement{A<:Algebra}
