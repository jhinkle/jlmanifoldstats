function loadBooksteinRatData()
    # load bookstein rat data, return a Vector of 2x8 Arrays, and the associated times
        
    #  Bookstein rat data
    dim = 2;
    D = readcsv("booksteinratdata.csv")
    # we throw out the three incomplete datasets

    skips=[3,13,20] # the real skippers (incomplete datasets)
    #skips=[2:21] # only do one rat!

    keeps=repmat([true],21,1)
    keeps[skips] = false

    y = Array[]
    t = Real[]
    for i=1:size(D,1)
        if keeps[D[i,1]]
            # skip the rows whose first column match those in skips
            #push(t,D[i,2]) # actual time in days
            push!(t,((i-1) % 8)+1) # artificial time (equal spacing)
            yi = [D[i,3:2:end]; D[i,4:2:end]]
            push!(y,yi)
        end
    end

    (y,t)
end
