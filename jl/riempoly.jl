# This file implements the forward Euler integration and discretized
# adjoint Jacobi system for Riemannian polynomial regression first
# developed in Hinkle et al. 2012: http://arxiv.org/abs/1201.2395

include("geometry.jl")

function
    polyIntegrate{M<:RiemannianManifold,P<:ManifoldPoint,V<:TangentVector}(::Type{M},x0::P,
    v0::Vector{V}, numSteps, stepsize)
    ppath = P[] # this will be the entire path
    p = x0
    v = v0 # just a single time point collection of vectors

    k=length(v0) # order of polynomial

    for iter = 1:numSteps
        vel = v[1] # don't want to update our velocity by mistake
        
        v[1:k-1] += stepsize*v[2:k] # increment for ode
        for i=1:k
            # parallel translate and add
            v[i] = partrans(p,stepsize*vel,v[i])
        end
        push(ppath, expmap(p,stepsize*vel))
    end
    ppath
end

# default arguments
function polyIntegrate{M<:RiemannianManifold,P<:ManifoldPoint,V<:TangentVector}(::Type{M},x0::P,v0::Vector{V})
    polyIntegrate(M,x0,v0,100,.01)
end


function polyRegress{M<:RiemannianManifold,P<:ManifoldPoint,V<:TangentVector}(::Type{M},y::Vector{P},x0guess::P,v0guess::Vector{V},Niter,stepsizeGD,numSteps,stepsize)
    # TODO: Fix these argument names!
    # Do polynomial regression to find optimal initial conditions for polynomial
    # This function requires implementations of expmap,partrans,and curvtensor
    x0 = x0guess
    v0 = v0guess

    lambda = V[]
    V0 = newTangentVector(M)
    for i=1:k+1
        lambda[i] = V0
    end
    for iter = 1:Niter
        # integrate forward all the way and hold on to the path
        ppath = polyIntegrate(M,x0,v0,numSteps,stepsize)

        # initialize adjoint variables to zero
        for i=1:k+1
            lambda[i] = V0
        end

        # integrate adjoint equations backwards
            # if we pass over a data point then add its log map to lambda(0)

        # when we get to zero, exponential map and parallel translate to update
        # parameters
    end
    # return parameters
end
