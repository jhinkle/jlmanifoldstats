#require("kendallSSregression.jl")
#require("nsphere.jl")

function permTestAgainstMean(y::Vector{NSpherePoint},t,yind,mu::NSpherePoint,SSEmu,k,Nperms,Niter)
    # do permutation test against the no-trend hypothesis (given Frechet mean)

    # NOTE: for this test we only need to permute yind, leaving the data in place

    zerovec = NSphereTangentVector(zeros(size(mu.p)))
    if k==1
        zeroIC = [zerovec] # squeeze will make this a scalar otherwise
    else
        zeroIC = squeeze(repmat([zerovec],k,1)) # initial conditions for each regression
    end

    (p0unperm,vi0unperm,SSEunperm) = polynomialRegression(y,t,yind,mu,zeroIC,Niter)
    R2unperm = 1- SSEunperm[end]/SSEmu
    R2list = [R2unperm]

    for iperm = 1:Nperms
        println("Regressing for permutation $iperm...")

        # generate random permutations of yind
        yindp = yind[randperm(length(yind))]

        # for each permutation, do regression of order k and save R^2
        # TODO: use nice initial conditions
        (p0perm,vi0perm,SSEperm) = polynomialRegression(y,t,yindp,mu,zeroIC,Niter,SSEunperm[end])
        R2perm = 1 - SSEperm[end]/SSEmu
        push(R2list,R2perm)
        pval = count(map(r->r>=R2unperm,R2list))/length(R2list)
        println("R2=$R2perm p-value ($(length(R2list)) samples)=$pval")
    end

    p = count(map(r->r>=R2unperm,R2list))/length(R2list)
    (p,R2list)
end

function permTestAgainstMean(y::Vector{KendallShape},t,yind,mu::KendallShape,SSEmu,k,Nperms,Niter)
    # do permutation test against the no-trend hypothesis (given Frechet mean)

    # NOTE: for this test we only need to permute yind, leaving the data in place

    zerovec = KendallTangentVector(zeros(size(mu.p)))
    if k==1
        zeroIC = [zerovec] # squeeze will make this a scalar otherwise
    else
        zeroIC = squeeze(repmat([zerovec],k,1)) # initial conditions for each regression
    end

    (p0unperm,vi0unperm,SSEunperm) = polynomialRegression(y,t,yind,mu,zeroIC,Niter)
    R2unperm = 1- SSEunperm[end]/SSEmu
    R2list = [R2unperm]

    for iperm = 1:Nperms
        println("Regressing for permutation $iperm...")

        # generate random permutations of yind
        yindp = yind[randperm(length(yind))]

        # for each permutation, do regression of order k and save R^2
        # TODO: use nice initial conditions
        (p0perm,vi0perm,SSEperm) = polynomialRegression(y,t,yindp,mu,zeroIC,Niter)
        R2perm = 1 - SSEperm[end]/SSEmu
        push(R2list,R2perm)
        pval = count(map(r->r>=R2unperm,R2list))/length(R2list)
        println("R2=$R2perm p-value ($(length(R2list)) samples)=$pval")
    end

    p = count(map(r->r>=R2unperm,R2list))/length(R2list)
    (p,R2list)
end

function permutePointPair{T<:NSpherePoint}(y::T,x0::T,x1::T)
    # given two points x0 and x1, determine the natural mapping between them and
    # apply it to y

    # this is easiest on the sphere
    x0s = x0.p[:] # implies copy
    x1s = x1.p[:]
    ys  = y.p[:]

    ## logmap
    c = dot(x0s,x1s) # dot product is cosine of angle (since both unit norm)
    t = acos(c)
    v = (x1s-c*x0s)
    if norm(v) < 1e-9
        return deepcopy(y) # x0 == x1
    end
    v = v/norm(v) # t*v is logmap(x0s,x1s)

    ## expmap
    # write y in x0-v-orth coordinates
    y0 = dot(ys,x0s)
    yv = dot(ys,v)
    yorth = ys - y0*x0s - yv*v

    # now just rotate in that plane
    s = sin(t)
    yrot = yorth + (c*y0 - s*yv)*x0s + (s*y0 + c*yv)*v

    projectToSphere(yrot)
end


function permutePointPair(y::KendallShape,x0::KendallShape,x1::KendallShape)
    # given two points x0 and x1, determine the natural mapping between them and
    # apply it to y
    
    x1a = alignShapes(x0,x1)
    ya = alignShapes(x0,y)

    # this is easiest on the sphere
    x0s = x0.p[:] # implies copy
    x1s = x1a.p[:]
    ys  = ya.p[:]

    ## logmap
    c = dot(x0s,x1s) # dot product is cosine of angle (since both unit norm)
    t = acos(c)
    v = (x1s-c*x0s)
    if norm(v) < 1e-9
        return deepcopy(y) # x0 == x1
    end
    v = v/norm(v) # t*v is logmap(x0s,x1s)

    ## expmap
    # write y in x0-v-orth coordinates
    y0 = dot(ys,x0s)
    yv = dot(ys,v)
    yorth = ys - y0*x0s - yv*v

    # now just rotate in that plane
    s = sin(t)
    yrot = yorth + (c*y0 - s*yv)*x0s + (s*y0 + c*yv)*v

    yn = makeKendallShape(reshape(yrot,size(y.p))) # just to be sure we stay normalized
    alignShapes(x1,yn)
end

function permutePointList{T<:Integer,S}(y::Vector{S},x::Vector{S},p::Vector{T})
    # use the natural action of SO(n-1) on kendall preshape space S^{n-2} to
    # permute points y given actions determined by pairs of points in x, to
    # satisfy the permutation given by p

    y0 = deepcopy(y)
    yperm = deepcopy(y)
    for i = 1:length(p)
        yperm[i] = permutePointPair(y0[i],x[i],x[p[i]])
    end
    yperm
end

function permTestAgainstCurve{T}(y::Vector{T},t,yind,p0,vi0,SSEmu,k,Nperms,Niter)
    # given H_0-fit points x and some data, permute the points using the action
    # of SO(n-1) and refit to get a p-value that an order k model significantly
    # improves the fit

    #assert(k > length(vi0))

    #SSEunperm = SSEmu*(1-R2unperm)
    #println("Permutation testing with SSEmu=$SSEmu and SSEunperm=$SSEunperm")

    # first integrate to get the baseline curve
    (xt,) = integratePolynomial(p0,vi0,t)
    x = xt[yind]

    # include the test statistic (so that small sample bias is fair)
    #R2list = [R2unperm]

    # initial conditions will be those of the lower order
    zerovec = typeof(vi0[1])(zeros(size(p0.p)))
    regIC = squeeze(vcat(vi0,repmat([zerovec],k-length(vi0),1)))
    #regIC = deepcopy(vi0)

    # first run on unpermuted data
    (p0unperm,vi0unperm,SSEunperm) = polynomialRegression(y,t,yind,p0,regIC,Niter)
    R2unperm = 1- SSEunperm[end]/SSEmu
    R2list = [R2unperm]

    for iperm = 1:Nperms
        print("Regressing for permutation $iperm, SSEunperm=$(SSEunperm[end])...")
        if iperm > 1
            SSElist = SSEmu*(1-R2list)
            println("SSE mean(sigma)=$(mean(SSElist))($(sqrt(sum((SSElist-mean(SSElist)).^2)/(length(SSElist-1)))))")
        end

        # generate random permutations
        p = randperm(length(yind))

        # permute points y given points x and p
        yperm = permutePointList(y,x,p)

        (p0perm,vi0perm,SSEperm) = polynomialRegression(yperm,t,yind[p],p0,regIC,Niter,SSEunperm[end])
        R2perm = 1 - SSEperm[end]/SSEmu
        push(R2list,R2perm)
        pval = count(map(r->r>=R2unperm,R2list))/length(R2list)
        println("R2=$R2perm p-value ($(length(R2list)) samples)=$pval")
        if false
            # histograms
            closeall()
            figure(iperm)
            c = CurveConf()
            c.plotstyle = "boxes"
            c.color = "blue"
            (xh,yh) = histdata(R2list[2:end],min(iperm,20))
            addcoords(xh,yh,c)
            a = AxesConf()
            a.title = "R^2 distribution"
            addconf(a)
            llplot()
        end
    end

    pval = count(map(r-> r >= R2unperm,R2list))/length(R2list)
    (pval,R2list)
end
