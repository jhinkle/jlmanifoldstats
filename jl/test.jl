#!/scratch/jacob/src/julia/julia

include("geometry.jl")
include("sphere.jl")
include("euclidean.jl")
include("frechetmean.jl")

rawq = Float64[1,0,0]
#qs = SpherePoint(rawq)
#print("qs = $(qs.p)\n")
q = newManifoldPoint(Sphere,rawq)
rawp = [0 0 1]
p = newManifoldPoint(Sphere,rawp)
rawv = [0,1,0]
v = newTangentVector(Sphere,rawv)

pv = newTangentBundlePoint(Sphere,p,v)

#print(distance(p,q))

print("The exponential map of $pv is $(expmap(pv))\n")
print("The log map of $q at point $p is $(logmap(p,q))\n")

# generate random data for log map
Npoints = 1000
ydata = SpherePoint[]
for i = 1:Npoints
    p = randn(3)
    p /= norm(p)
    #print("p=$p and typeof(p)=$(typeof(p)) and norm is $(norm(p))\n")
    push(ydata,newManifoldPoint(Sphere,p))
end

# get frechet mean
Niter = 100
stepsize = 0.1
tic()
xhat = frechetMean(Sphere,ydata,ydata[1],Niter,stepsize)
#xhat = frechetMean(Sphere,ydata,ydata[1])
toc()

# now test the euclidean frechet mean (using same points, why not)
ydataEuc = EuclideanPoint[]
for yd = ydata
    push(ydataEuc,newManifoldPoint(Euclidean,yd.p))
end
xhatEuc = frechetMean(Euclidean,ydataEuc,ydataEuc[1])

print("Intrinsic mean is $xhat, while extrinsic mean is $xhatEuc\n")

# TODO: load up the bookstein rat shape data and take the mean

# report results
