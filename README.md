# Manifold Statistics in Julia

This package provides routines for doing geodesic and polynomial regression
(c.f. [Hinkle et al.
2012](http://link.springer.com/chapter/10.1007%2F978-3-642-33712-3_1)), as well
as Frechet means, in the [Julia](http://julialang.org) programming
language.

## Which spaces are supported

Currently I've implemented sphere and Kendall shape space (2D) methods.  Julia
is really nice because multiple dispatch allows a lot of the higher level
functions like hypothesis testing and (at a certain level) regression itself to
be written in a very general style.

I plan to implement Lie group polynomial regression (which should be done
differently than for general Riemannian manifolds).

## Visualization

There is currently very limited visualization capability in Julia, but I have
played around with gaston.  Stay tuned..
